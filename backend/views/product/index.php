<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Products';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index box box-primary">
    <?php Pjax::begin(); ?>
    <div class="box-header with-border">
        <p><?= Html::a('Create Product', ['create'], ['class' => 'btn btn-success btn-flat']) ?></p>
        <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <div class="box-body table-responsive">

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            //'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                [
                    'class' => 'yii\grid\SerialColumn',
                    'options' => ['style' => 'width:10px;']
                ],
                'name',
                [ 
                    'attribute' => 'price',
                    'options' => ['style' => 'width:150px;']
                ],
                [
                    'attribute' => 'is_published',
                    'value' => function ($data) {
                        if ($data->is_published) {
                            return 'Yes';
                        }
                        return 'No';
                    },
                    'options' => ['style' => 'width:150px;']

                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'options' => ['style' => 'width:150px;']
                ],
            ],
        ]); ?>
    </div>
    <?php Pjax::end(); ?>
</div>