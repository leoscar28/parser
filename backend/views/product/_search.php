<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\ProductSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'price')->input('number') ?>

    <?= $form->field($model, 'is_published')->checkbox() ?>

    <div class="form-group text-right">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary  btn-flat']) ?>
        <?= Html::resetButton('Reset', ['class' =>'btn btn-default  btn-flat']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
