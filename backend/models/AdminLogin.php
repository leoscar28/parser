<?php

namespace backend\models;

use Yii;
use common\models\LoginForm;

/**
 * Admin login
 */
class AdminLogin extends LoginForm
{

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, Yii::t('app', 'Incorrect username or password'));
            } else {
                $userRoles = Yii::$app->authManager->getRolesByUser($user->id);
                if (!(array_key_exists('administrator', $userRoles) || array_key_exists('moderator', $userRoles))) {
                    $this->addError($attribute, Yii::t('app', 'Admin access only'));
                }
            }
        }
    }
}
