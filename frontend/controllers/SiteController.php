<?php

namespace frontend\controllers;

use yii\web\Controller;
use common\models\Product;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * Site controller
 */
class SiteController extends Controller
{


    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {

        $products = Product::find()->where(['is_published' => Product::PUBLISHED_TRUE])->all();

        $data = ArrayHelper::toArray($products, [
            'common\models\Product' => [
                'id',
                'name',
                'description' => 'desc',
                'price',
                'lowPrice'
            ],
        ]);

        ArrayHelper::multisort($data, ['lowPrice'], [SORT_DESC]);

        return $this->render('index', [
            'products' => $data
        ]);
    }


    public function actionProduct($id)
    {
        return $this->render('view', [
            'model' => $this->findProduct($id),
        ]);
    }


    protected function findProduct($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
