<?php

use aki\vue\Vue;
use aki\vue\VueComponent;
use aki\vue\VueRouter;

use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = Yii::$app->name;
?>
<div class="site-index">

    <?php Vue::begin([
        'id' => "vue-app",
        'data' => [
            'products' => $products,
            'search' => ''
        ],

    ]); ?>
    <div class="panel panel-success">
        <div class="panel-heading">Search</div>
        <div class="panel-body">
            <input type="text" v-model="search" class="form-control" />
        </div>

    </div>


    <div class="row">
        <div v-for="product in products" v-show="product.name.toLowerCase().includes(search.toLowerCase()) || !search" class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading text-center">{{product.name}}</div>
                <div class="panel-body">
                    <p>Description: {{product.descripiton}}</p>
                    <p>Price: {{product.lowPrice}}</p>
                    <div class="text-right"><a :href="'site/product?id='+product.id" class="btn btn-sm btn-primary">View</a></div>
                </div>
            </div>

        </div>
    </div>


    <?php Vue::end(); ?>
</div>