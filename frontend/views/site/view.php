<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Product */

$this->title = $model->name;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-view">
   
    <div class="table-responsive no-padding">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'name',
                'desc:ntext',
                'lowPrice',
                'is_published',
            ],
        ]) ?>
    </div>
</div>
