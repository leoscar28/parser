<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property string $name
 * @property string|null $desc
 * @property int $price
 * @property int|null $is_published
 *
 * @property ProductPrice[] $productPrices
 */
class Product extends \yii\db\ActiveRecord
{

    const PUBLISHED_FALSE = 0;
    const PUBLISHED_TRUE = 1;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'price'], 'required'],
            [['desc'], 'string'],
            [['price', 'is_published'], 'integer'],
            ['is_published', 'default', 'value' => self::PUBLISHED_FALSE],
            ['is_published', 'in', 'range' => [self::PUBLISHED_FALSE, self::PUBLISHED_TRUE]],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'desc' => 'Desc',
            'price' => 'Price',
            'is_published' => 'Is Published',
        ];
    }

    /**
     * Gets query for [[ProductPrices]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductPrices()
    {
        return $this->hasMany(ProductPrice::class, ['product_id' => 'id']);
    }


    public function deleteOldPrices()
    {
        $prices = $this->productPrices;

        foreach ($prices as $price) {
            $price->delete();
        }

        return true;
    }


    public function getLowPrice()
    {
        $lowest = ProductPrice::find()->where(['product_id' => $this->id])->andWhere(['<', 'price', $this->price])->orderBy(['price' => SORT_ASC])->one();

        $price =  ($lowest) ? $lowest->price : $this->price;

        return $price;
    }


}
