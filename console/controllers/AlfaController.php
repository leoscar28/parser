<?php

namespace console\controllers;

use common\models\Product;
use common\models\ProductPrice;
use GuzzleHttp\Client;
use yii\console\Controller;

class AlfaController extends Controller
{
    const SITE_URL = 'https://alfa.kz';
    

    public function actionParse()
    {

        $dbProducts = Product::find()->with(['productPrices'])->all(); // Получаем все продукты из нашей БД

        foreach ($dbProducts as $dbProduct) {
            
            $dbProductNameLower = mb_strtolower($dbProduct->name);
            
            $qString = '/q/' . str_replace(' ', '%20', $dbProductNameLower);  // Формируем поисковый запрос
            $prices = []; // Массив для новых цен
           
            $count = $this->getSearchPagesCount($qString); // Получаем количество страниц пагинации по указанному роуту
            
            echo 'Товар - ' . $dbProduct->name . '. Цен в базе данных: ' . count($dbProduct->productPrices) . "\n";
            for ($i = 0; $i < $count + 1; $i++) { // Пробегаем по всем страницам полученным в поиске
                $client = new Client();
                $resourse = ($i == 0) ? $client->request('GET', self::SITE_URL . $qString) : $client->request('GET', self::SITE_URL . $qString . '?page=' . $i . '#products');
                $body = $resourse->getBody();
                $document = \phpQuery::newDocumentHTML($body);
                $products = $document->find("div.product-item");
                
                foreach ($products as $product) {
                    $pq = pq($product);
                    if (trim(mb_strtolower(strip_tags($pq->find('div.title h2')->html()))) == $dbProductNameLower) { // Если название товара имеет точное совпадение с нашим, то добавляем цену в массив
                        $price = $pq->find('div.price span.num')->html() ?? false;
                        $prices[] = intval(str_replace(' ', '', $price));
                    }
                }
            }
            if (count($prices) > 0) { // Если есть новые цены

                echo 'Для товара ' . $dbProduct->name . ' найдено новых цен: ' . count($prices) . "\n";

                if ($dbProduct->deleteOldPrices()) { // Удалаяем старые цены
                    echo 'Для товара ' . $dbProduct->name . ' удалены старые цены.'  . "\n";
                }

                asort($prices); // Сортировка цен по возрастанию
                $cutPrices =  array_slice($prices, 0, 5); // Оставляем 5 самых дешевых цен

                foreach ($cutPrices as $item) {  // Добавляем новые цены
                    $newProductPrice = new ProductPrice(['product_id' => $dbProduct->id, 'price' => $item]);
                    $newProductPrice->save();
                }
            } else { // Новых цен не обнаружено или товар не найден
                echo 'Для товара ' . $dbProduct->name . ' не цены не найдены.' . "\n";
            }
        }
    }
   
  /* Функция считает сколько страниц в пагинации по указанному $route*/

    private function getSearchPagesCount($route)
    {
        $client = new Client();
        $resourse = $client->request('GET', self::SITE_URL .  $route);
        $body = $resourse->getBody();
        $document = \phpQuery::newDocumentHTML($body);
        $paginationItems = $document->find("ul.pagination li");
        $count = 0;
        foreach ($paginationItems as $item) {
            $item = pq($item);
            $number = intval($item->find('a')->html() ?? false);
            if ($number > $count) {
                $count = $number;
            }
        }
        return $count;
    }

}
