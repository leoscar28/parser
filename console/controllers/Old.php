<?php

namespace console\controllers;

use common\models\Product;
use common\models\ProductPrice;
use GuzzleHttp\Client;
use yii\console\Controller;

class AlfaController extends Controller
{
    const SITE_URL = 'https://alfa.kz';

    const ROUTES = ['pc', 'parts', 'peripherals', 'network', 'phones', 'electronics', 'household'];



    public function actionParse()
    {

        $dbProducts = Product::find()->with(['productPrices'])->all();


        foreach ($dbProducts as $dbProduct) {
            $qString = '/q/' . str_replace(' ', '%20', strtolower($dbProduct->name));
            $prices = [];
            $count = $this->getSearchPagesCount($qString);
            echo 'Товар - ' . $dbProduct->name . '. Цен в базе данных: ' . count($dbProduct->productPrices) . "\n";
            for ($i = 0; $i < $count + 1; $i++) {
                $client = new Client();
                $resourse = ($i == 0) ? $client->request('GET', self::SITE_URL . $qString) : $client->request('GET', self::SITE_URL . $qString . '?page=' . $i . '#products');
                $body = $resourse->getBody();
                $document = \phpQuery::newDocumentHTML($body);
                $products = $document->find("div.product-item");

                foreach ($products as $product) {
                    $pq = pq($product);
                    if (trim(strtolower(strip_tags($pq->find('div.title h2')->html()))) == strtolower($dbProduct->name)) {
                        $price = $pq->find('div.price span.num')->html() ?? false;
                        $prices[] = intval(str_replace(' ', '', $price));
                    }
                }
            }
            if (count($prices) > 0) {

                echo 'Для товара ' . $dbProduct->name . ' найдено новых цен: ' . count($prices) . "\n";

                if ($dbProduct->deleteOldPrices()) {
                    echo 'Для товара ' . $dbProduct->name . ' удалены старые цены.'  . "\n";
                }

                asort($prices);
                $cutPrices =  array_slice($prices, 0, 5);

                foreach ($cutPrices as $item) {
                    $newProductPrice = new ProductPrice(['product_id' => $dbProduct->id, 'price' => $item]);
                    $newProductPrice->save();
                }
            } else {
                echo 'Для товара ' . $dbProduct->name . ' не цены не найдены.' . "\n";
            }
        }
    }

   

    private function getSearchPagesCount($route)
    {
        $client = new Client();
        $resourse = $client->request('GET', self::SITE_URL .  $route);
        $body = $resourse->getBody();
        $document = \phpQuery::newDocumentHTML($body);
        $paginationItems = $document->find("ul.pagination li");
        $count = 0;
        foreach ($paginationItems as $item) {
            $item = pq($item);
            $number = intval($item->find('a')->html() ?? false);
            if ($number > $count) {
                $count = $number;
            }
        }
        return $count;
    }


    public function actionParse2()
    {

        $productsArr = [];

        foreach (self::ROUTES as $route) {
            echo $route . "\n";
            $count = $this->getPagesCount($route);

            for ($i = 0; $i < $count + 1; $i++) {
                $client = new Client();
                $resourse = ($i == 0) ? $client->request('GET', self::SITE_URL . '/' . $route . '/') : $client->request('GET', self::SITE_URL . '/' . $route . '?page=' . $i . '#products');
                $body = $resourse->getBody();
                $document = \phpQuery::newDocumentHTML($body);

                $products = $document->find("div.product-item");

                foreach ($products as $product) {
                    $pq = pq($product);
                    $title = $pq->find('div.title h2')->html() ?? false;
                    $price = $pq->find('div.price span.num')->html() ?? false;
                    if ($title) {
                        $productsArr[] = ['title' => $title, 'price' => intval(str_replace(' ', '', $price))];
                    }
                }
            }
        }

        $prices = [];

        foreach ($productsArr as $product) {

            if (Product::find()->where(['name' => $product['title']])->exists()) {
                echo 'Данный товар уже есть в БД: ' . $product['title'] . "\n";
                $prices[$product['title']][] = $product['price'];
            } else {
                $newProduct = new Product(['name' => $product['title'], 'price' => $product['price']]);
                $newProduct->save();
            }
        }

        foreach ($prices as $key => $price) {
            $product =  Product::find()->where(['name' => $key])->one();
            if ($product) {
                asort($price);
                $count = count($price) > 5 ? 5 : count($price);
                for ($i = 0; $i < $count; $i++) {
                    $newProductPrice = new ProductPrice(['product_id' => $product->id, 'price' => $price[$i]]);
                    $newProductPrice->save();
                }
            }
        }
    }

    private function getPagesCount($route)
    {
        $client = new Client();
        $resourse = $client->request('GET', self::SITE_URL . '/' . $route . '/');
        $body = $resourse->getBody();
        $document = \phpQuery::newDocumentHTML($body);
        $paginationItems = $document->find("ul.pagination li");
        $count = 1;
        foreach ($paginationItems as $item) {
            $item = pq($item);
            $number = intval($item->find('a')->html() ?? false);
            if ($number > $count) {
                $count = $number;
            }
        }
        return $count;
    }
}
