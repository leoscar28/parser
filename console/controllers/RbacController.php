<?php
namespace console\controllers;

use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        $user = $auth->createRole('user');
        $auth->add($user);
        
        $moderator = $auth->createRole('moderator');
        $auth->add($moderator);       
       
        $administrator = $auth->createRole('administrator');
        $auth->add($administrator);
       
        $auth->assign($administrator, 1);        
    }
}