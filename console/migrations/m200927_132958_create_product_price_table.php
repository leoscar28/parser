<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%product_price}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%product}}`
 */
class m200927_132958_create_product_price_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%product_price}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'price' => $this->integer()->notNull(),
        ]);

        // creates index for column `product_id`
        $this->createIndex(
            '{{%idx-product_price-product_id}}',
            '{{%product_price}}',
            'product_id'
        );

        // add foreign key for table `{{%product}}`
        $this->addForeignKey(
            '{{%fk-product_price-product_id}}',
            '{{%product_price}}',
            'product_id',
            '{{%product}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%product}}`
        $this->dropForeignKey(
            '{{%fk-product_price-product_id}}',
            '{{%product_price}}'
        );

        // drops index for column `product_id`
        $this->dropIndex(
            '{{%idx-product_price-product_id}}',
            '{{%product_price}}'
        );

        $this->dropTable('{{%product_price}}');
    }
}
